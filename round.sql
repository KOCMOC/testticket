/*
SQLyog Community Edition- MySQL GUI v5.22a
Host - 5.6.22-log : Database - test
*********************************************************************
Server version : 5.6.22-log
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `test`;

USE `test`;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `round` */

DROP TABLE IF EXISTS `round`;

CREATE TABLE `round` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `x` smallint(4) NOT NULL COMMENT 'абсцисса центра',
  `y` smallint(4) NOT NULL COMMENT 'ордината центра',
  `radius` smallint(5) NOT NULL,
  `color` varchar(6) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `round` */

insert  into `round`(`id`,`x`,`y`,`radius`,`color`,`message`) values (1,150,70,50,'ff6600','я апельсинко'),(2,200,100,20,'ffff00','я лимонко'),(3,500,200,70,'ff0000','а я томат');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
