<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%round}}".
 *
 */
class Round extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%round}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['x', 'y', 'radius', 'color', 'message'], 'required'],
            ['message', 'string', 'max' => 255],
            ['color', 'string', 'length' => [6]],
            [['x', 'y'], 'integer', 'min' => -5000, 'max' => 5000],
            ['radius', 'integer', 'min' => 0, 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'x' => 'Абсцисса',
            'y' => 'Ордината',
            'radius' => 'Радиус',
            'color' => 'Цвет',
            'message' => 'Сообщение',
        ];
    }
}
