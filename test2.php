<?php

class point {	private $x;
	private $y;
	private $relations;

	public function __construct($x = false, $y = false) {    	$this->x = $x;
    	$this->y = $y;
    	$this->relations = ['left'=>false, 'right'=>false];	}

	public function setX($x) {		$this->x = $x;
		return $this;	}

	public function setY($y) {		$this->y = $y;
		return $this;	}

	public function getX() {		return $this->x;	}

	public function getY() {
		return $this->y;
	}

	public function setLeftRelation(point $point) {    	$this->relations['left'] = $point;
    	return $this;	}
	public function setRightRelation(point $point) {
    	$this->relations['right'] = $point;
    	return $this;
	}
	public function getLeftRelation() {
    	return $this->relations['left'];
	}
	public function getRightRelation() {
    	return $this->relations['right'];
	}}

class chain {	private $points;

	public function __construct() {		$this->points = [];	}

	public function setPoints(array $points) {    	while($point = array_shift($points)) {
        	$this->appendPoint($point);    	}
    	return $this;	}

	public function appendPoint($point) {
		if(is_array($point) and sizeof($point) == 2) {			$_point = new point($point[0], $point[1]);
	   		if(sizeof($this->points) > 0) {
	   			$last_point = end($this->points);
	   			$_point->setLeftRelation($last_point);
	   		}	    	array_push($this->points, $_point);
	   		if(isset($last_point)) {
	   			$last_point->setRightRelation(end($this->points));
	   		}
   		}
   		return $this;	}

	public function getLength() {
		$length = 0;		foreach($this->points as $point) {
			$rightRelation = $point->getRightRelation();			if($rightRelation !== false) {
				$x = $rightRelation->getX() - $point->getX();
				$y = $rightRelation->getY() - $point->getY();
                $length += sqrt($x*$x+$y*$y);
			}		}
		return $length;	}}

$chain = new chain;
$chain->setPoints([[1, 2], [0, 4], [5, 9]]);
$chain->appendPoint([10, 16]);

echo $chain->getLength();

?>