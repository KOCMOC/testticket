select concat(a.first_name, ' ', a.last_name) actor, count(1) as cnt, c.name cname, concat(ROUND(count(1)/(SELECT count(1) FROM film_actor WHERE film_actor.actor_id=a.actor_id)*100, 4), '%') from film_actor fa
  join actor a on (a.actor_id=fa.actor_id)
  join film_category fc on (fc.film_id=fa.film_id)
  join category c on (c.category_id=fc.category_id)
  group by c.category_id, a.actor_id ORDER BY a.actor_id, cname