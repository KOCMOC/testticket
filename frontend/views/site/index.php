<?php

/* @var $this yii\web\View */

$this->title = 'Круги на полях. Разным цветом. С разными приветствиями этому миру.';
?>
<div class="site-index">
	<canvas id="round" width="1000" height="500"></canvas>
</div>
<script type="text/javascript">
    <?php foreach($rounds as $round): ?>
	goRound(<?=$round->x?>, <?=$round->y?>, <?=$round->radius?>, '<?=$round->color?>', '<?=$round->message?>');
	<?php endforeach; ?>
	document.getElementById('round').addEventListener('click',canvasClick,false);
</script>
