var rounds = [], num = 0;

function goRound(x, y, radius, color, message)
{
	var canvas = document.getElementById('round');
    var obCanvas = canvas.getContext('2d');

    obCanvas.beginPath();
    obCanvas.arc(x, y, radius, 0, 2*Math.PI);
    obCanvas.fillStyle = '#'+color;
    obCanvas.fill();

    rounds[num] = {x:x, y:y, radius:radius, message:message};
    num++;

}
function canvasClick()
{
	var canvas = document.getElementById('round'), realAlert = false;
	var pos = findOffset(canvas);	mouseX = event.clientX - pos.x;
    mouseY = event.clientY - pos.y;

    for(var i=0; i<num; i++) {
	    var context = canvas.getContext('2d');
	    context.beginPath();
	    context.arc(rounds[i].x, rounds[i].y, rounds[i].radius, 0, 2*Math.PI);    	if(context.isPointInPath(mouseX, mouseY)) {    		realAlert = rounds[i].message;    	}    }

    if(realAlert !== false) {    	alert(realAlert);    }}
function findOffset(obj) {
    var curX = curY = 0;
    if (obj.offsetParent) {
        do {
            curX += obj.offsetLeft;
            curY += obj.offsetTop;
        } while (obj = obj.offsetParent);
    return {x:curX,y:curY};
    }
}