<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Round */

$this->title = 'Создать круг';
$this->params['breadcrumbs'][] = ['label' => 'Круги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="round-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
